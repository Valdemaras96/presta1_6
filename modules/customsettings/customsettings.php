<?php

if (!defined('_PS_VERSION_'))
    exit;

class customsettings extends Module
{

    protected $_html = '';

    public function __construct()
    {
        $this->name = 'customsettings';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Valdemaras Ambraziunas';
        $this->need_instance = 0;

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Custom Settings');
        $this->description = $this->l('Settings which could not be added elsewhere.');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        if (!parent::install()

        )
            return false;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }


    public function getContent()
    {
        $this->_html = ' <h2>'.$this->displayName.'</h2>';
        $this->_postProcess();
        $this->_html .= $this->renderShopCommentsForm();
        return $this->_html;
    }

    protected function _postProcess()
    {
        if(Tools::isSubmit('submitSettings'))
        {
            $settings_values = $this->getSettingsFieldsValues();
            foreach($settings_values as $key => $value)
            {
                if(Tools::getValue($key))
                {
                    Configuration::updateValue($key, Tools::getValue($key));
                }
            }

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true).'&conf=6&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'');
        }
    }


    protected function renderShopCommentsForm()
    {
        $settings_fields_form = $this->getSettingsFormFields();
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getSettingsFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        $settings_form = $helper->generateForm(array($settings_fields_form));
        return $settings_form;
    }

    protected function getSettingsFieldsValues()
    {
        $fields = array();
        $fields['MY_CUSTOM_SETTING'] = Configuration::get('MY_CUSTOM_SETTING');


        return $fields;
    }

    protected function getSettingsFormFields()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Custom Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                    'type' => 'text',
                    'label' => 'Some text: ',
                    'name' => 'MY_CUSTOM_SETTING',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'name'=>'submitSettings'
                )
            )
        );
    }
}